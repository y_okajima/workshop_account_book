package com.key.pleinair.accountbook.utils;

import android.text.TextUtils;

/**
 *
 */
public class DataDetail {
    private int id = 0;
    private String category = "";
    private String value = "";
    private String date = "";
    private int year = 0;
    private int month = 0;
    private int day = 0;

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMonth() {
        return month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getDay() {
        return day;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    /**
     * 未入力項目チェック
     * @return
     */
    public boolean isEmpty() {
        if(TextUtils.isEmpty(category) || TextUtils.isEmpty(value)) {
            return true;
        }
        return false;
    }
}
