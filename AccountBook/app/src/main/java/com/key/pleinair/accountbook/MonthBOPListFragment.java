package com.key.pleinair.accountbook;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.key.pleinair.accountbook.utils.ConstDef;
import com.key.pleinair.accountbook.utils.DBManager;
import com.key.pleinair.accountbook.utils.DBOpenHelper;
import com.key.pleinair.accountbook.utils.DataDetail;
import com.key.pleinair.accountbook.utils.OnListChangeListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MonthBOPListFragment extends Fragment implements OnListChangeListener {

    private static ArrayList<DataDetail> incomeList = new ArrayList<>();
    private static ArrayList<DataDetail> expensesList = new ArrayList<>();
    private TabLayout tabLayout;
    private CustomAdapter customAdapter;
    private ViewPager pager;
    int year;
    int month;

    private static final String TAG = MonthBOPListFragment.class.getSimpleName();

    public MonthBOPListFragment() {
        // Required empty public constructor
    }

    public static MonthBOPListFragment newInstance(Bundle bundle) {
        MonthBOPListFragment fragment = new MonthBOPListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.month_boplist_fragment, container, false);

        Bundle bundle = getArguments();
        year = bundle.getInt(ConstDef.INTENT_YEAR);
        month = bundle.getInt(ConstDef.INTENT_MONTH);

        TextView title = (TextView) v.findViewById(R.id.month_title);
        ImageView leftArrow = (ImageView) v.findViewById(R.id.left_arrow);
        ImageView rightArrow = (ImageView) v.findViewById(R.id.right_arrow);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                month = month - 1;
                if(month == 0) {
                    month = 12;
                    year = year - 1;
                }
                Bundle bundle = new Bundle();
                bundle.putInt(ConstDef.INTENT_YEAR, year);
                bundle.putInt(ConstDef.INTENT_MONTH, month);
                MonthBOPListFragment fragment = new MonthBOPListFragment().newInstance(bundle);
                getFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment).commit();
            }
        });

        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                month = month + 1;
                if(month > 12) {
                    month = 1;
                    year = year + 1;
                }
                Bundle bundle = new Bundle();
                bundle.putInt(ConstDef.INTENT_YEAR, year);
                bundle.putInt(ConstDef.INTENT_MONTH, month);
                MonthBOPListFragment fragment = new MonthBOPListFragment().newInstance(bundle);
                getFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment).commit();
            }
        });

        title.setText(getString(R.string.month_bop_title, year, month));
        String table = ConstDef.TABLE_NAME;
        DBOpenHelper helper = new DBOpenHelper(getContext(), table);
        SQLiteDatabase db = helper.getWritableDatabase();
        DBManager manager = new DBManager(db, table);
        incomeList = manager.selectMonth(year, month, ConstDef.DB_TYPE_INCOME);
        expensesList = manager.selectMonth(year, month, ConstDef.DB_TYPE_EXPENSES);
        db.close();

        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        pager = (ViewPager) v.findViewById(R.id.pager);
        //pager.setOffscreenPageLimit(3);//タブ３つは破棄せず再利用
        // 入れ子の場合getFragmentMaagerではなくgetChildFragmentManagerを渡す
        customAdapter = new CustomAdapter(getChildFragmentManager());
        pager.setAdapter(customAdapter);
        tabLayout.setupWithViewPager(pager); //スワイプするとタブのステータスが変わる

        return v;
    }


    @Override
    public void onListSelectedChanged(String s) {
        //TextFragment fragment = (TextFragment)adapter.findFragmentByPosition(pager, 1);
        //fragment.setText(s);
        //pager.setCurrentItem(1);
    }

    // カスタムタブ
    public class CustomAdapter extends FragmentPagerAdapter {

        private String[] pageTitle = {"収入", "支出"};//タブのタイトル

        public CustomAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new incomeListFragment();
                case 1:
                    return new expensesListFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return pageTitle.length;//タブ数
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pageTitle[position]; //タブのタイトル
        }

        public Fragment findFragmentByPosition(ViewPager viewPager,
                                               int position) {
            return (Fragment) instantiateItem(viewPager, position);
        }
    }


    public static class incomeListFragment extends ListFragment {

        private ArrayAdapter<DataDetail> incomeAdapter;
        private List<DataDetail> list;
        /*
        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            incomeAdapter = new ListAdapter(getActivity(), incomeList);
            setListAdapter(incomeAdapter);
        }
        */

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            incomeAdapter = new ListAdapter(getActivity(), incomeList);
            setListAdapter(incomeAdapter);
        }

        private class ListAdapter extends ArrayAdapter<DataDetail> {
            private LayoutInflater inflater;

            public ListAdapter(Context context, List<DataDetail> objects) {
                super(context, 0, objects);
                inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            public View getView(int position, View convertView, ViewGroup parent) {
                ItemHolder holder;
                // パーティーメンバーリスト用アイテムと接続
                if(convertView == null) {
                    convertView = inflater.inflate(R.layout.month_list_item, null);
                }
                holder = new ItemHolder();
                holder.categoryText = (TextView) convertView.findViewById(R.id.category_text);
                holder.moneyText = (TextView) convertView.findViewById(R.id.money_text);
                holder.deleteButton = (ImageView) convertView.findViewById(R.id.delete_button);

                final DataDetail item = getItem(position);
                holder.categoryText.setText(item.getCategory());
                holder.moneyText.setText(item.getValue());

                holder.deleteButton.setTag(position);
                holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = (int) v.getTag();
                        String table = ConstDef.TABLE_NAME;
                        DBOpenHelper helper = new DBOpenHelper(getContext(), table);
                        SQLiteDatabase db = helper.getWritableDatabase();
                        DBManager manager = new DBManager(db, table);
                        manager.deleteData(item.getId());
                        db.close();

                        incomeList.remove(position);
                        incomeAdapter.notifyDataSetChanged();
                    }
                });

                return convertView;
            }
        }
    }

    public static class expensesListFragment extends ListFragment {

        private ArrayAdapter<DataDetail> expensesAdapter;
        private List<DataDetail> list;
        /*
        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            expensesAdapter = new ListAdapter(getActivity(), expensesList);
            setListAdapter(expensesAdapter);
        }*/

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            expensesAdapter = new ListAdapter(getActivity(), expensesList);
            setListAdapter(expensesAdapter);
        }

        private class ListAdapter extends ArrayAdapter<DataDetail> {
            private LayoutInflater inflater;

            public ListAdapter(Context context, List<DataDetail> objects) {
                super(context, 0, objects);
                inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            public View getView(int position, View convertView, ViewGroup parent) {
                ItemHolder holder;
                // パーティーメンバーリスト用アイテムと接続
                if(convertView == null) {
                    convertView = inflater.inflate(R.layout.month_list_item, null);
                }
                holder = new ItemHolder();
                holder.categoryText = (TextView) convertView.findViewById(R.id.category_text);
                holder.moneyText = (TextView) convertView.findViewById(R.id.money_text);
                holder.deleteButton = (ImageView) convertView.findViewById(R.id.delete_button);

                final DataDetail item = getItem(position);
                holder.categoryText.setText(item.getCategory());
                holder.moneyText.setText(item.getValue());

                holder.deleteButton.setTag(position);
                holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = (int) v.getTag();
                        String table = ConstDef.TABLE_NAME;
                        DBOpenHelper helper = new DBOpenHelper(getContext(), table);
                        SQLiteDatabase db = helper.getWritableDatabase();
                        DBManager manager = new DBManager(db, table);
                        manager.deleteData(item.getId());
                        db.close();

                        expensesList.remove(position);
                        expensesAdapter.notifyDataSetChanged();
                    }
                });

                return convertView;
            }
        }
    }

    private static class ItemHolder {
        TextView categoryText;
        TextView moneyText;
        ImageView deleteButton;
    }

}
