package com.key.pleinair.accountbook.utils;

/**
 * Created by okajima
 */

public class ConstDef {
    //TODO　まとめたほうがいいかもね
    public static final String INTENT_DATE = "date";
    public static final String INTENT_YEAR = "year";
    public static final String INTENT_MONTH = "month";
    public static final String INTENT_DAY = "day";
    public static final String INTENT_USER = "user_name";

    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_VALUE = "value";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_DAY = "day";
    public static final String COLUMN_MONTH = "month";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_TYPE = "type";

    public static final String PREF_IN_CATEGORY = "income_category";
    public static final String PREF_EX_CATEGORY = "expenses_category";
    public static final String PREF_USER = "login_user";

    public static final int DB_TYPE_INCOME = 1;
    public static final int DB_TYPE_EXPENSES = 2;

    public static String USER_ID = "";
    public static String TABLE_NAME = "";

    public static void setUserId(String userId) {
        USER_ID = userId;
        TABLE_NAME = "BOP_" + userId;
    }
}
