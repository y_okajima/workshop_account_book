package com.key.pleinair.accountbook;


import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.key.pleinair.accountbook.utils.AlertDlg;
import com.key.pleinair.accountbook.utils.ConstDef;
import com.key.pleinair.accountbook.utils.DBManager;
import com.key.pleinair.accountbook.utils.DBOpenHelper;
import com.key.pleinair.accountbook.utils.DataDetail;
import com.key.pleinair.accountbook.utils.SharedPreferencesUtil;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InputDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InputDetailFragment extends Fragment {
    // TODO 入力後(追加後)の編集を削除以外不可にすればいいのでは？
    private IncomeListAdapter incomeListAdapter;
    private ExpensesListAdapter expensesListAdapter;
    private ArrayList<DataDetail> incomeList = new ArrayList<>();
    private ArrayList<DataDetail> expensesList = new ArrayList<>();
    private ArrayList<String> incomeCatList = new ArrayList<>();
    private ArrayList<String> expensesCatList = new ArrayList<>();

    private TextView inputDate;
    private Button saveButton;
    private TextView incomeTotal;
    private TextView expensesTotal;
    private ImageView incomeAddButton;
    private ImageView expensesAddButton;
    private ListView incomeListView;
    private ListView expensesListView;
    private LinearLayout incomeListWrapper;
    private LinearLayout expensesListWrapper;
    private int listItemHeight;

    private Context context;
    private String userId = "";
    private int year = 0;
    private int month = 0;
    private int day = 0;
    private int sumIncome = 0;
    private int sumExpenses = 0;

    public InputDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param bundle
     * @return A new instance of fragment InputDetailFragment.
     */
    public static InputDetailFragment newInstance(Bundle bundle) {
        InputDetailFragment fragment = new InputDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.input_detail_fragment, container, false);
        context = getActivity();

        incomeCatList = SharedPreferencesUtil.getCategory(context, ConstDef.PREF_IN_CATEGORY);
        expensesCatList = SharedPreferencesUtil.getCategory(context, ConstDef.PREF_EX_CATEGORY);

        inputDate = (TextView) v.findViewById(R.id.input_date);
        saveButton = (Button) v.findViewById(R.id.save_button);
        incomeTotal = (TextView) v.findViewById(R.id.income_total);
        expensesTotal = (TextView) v.findViewById(R.id.expenses_total);
        incomeListView = (ListView) v.findViewById(R.id.income_list);
        expensesListView = (ListView) v.findViewById(R.id.expenses_list);
        incomeAddButton = (ImageView) v.findViewById(R.id.income_add_button);
        expensesAddButton = (ImageView) v.findViewById(R.id.expenses_add_button);

        Bundle bundle = getArguments();
        inputDate.setText(bundle.getString(ConstDef.INTENT_DATE));
        userId = bundle.getString(ConstDef.INTENT_USER);
        year = bundle.getInt(ConstDef.INTENT_YEAR);
        month = bundle.getInt(ConstDef.INTENT_MONTH);
        day = bundle.getInt(ConstDef.INTENT_DAY);

        String table = ConstDef.TABLE_NAME;
        DBOpenHelper helper = new DBOpenHelper(context, table);
        SQLiteDatabase db = helper.getWritableDatabase();
        DBManager manager = new DBManager(db, table);
        incomeList = manager.selectDay(year, month, day, ConstDef.DB_TYPE_INCOME);
        expensesList = manager.selectDay(year, month, day, ConstDef.DB_TYPE_EXPENSES);
        db.close();

        incomeListAdapter = new IncomeListAdapter(context);
        incomeListView.setAdapter(incomeListAdapter);
        expensesListAdapter = new ExpensesListAdapter(context);
        expensesListView.setAdapter(expensesListAdapter);

        // listviewのサイズを動的に変更するためlistitemのサイズを取得
        // listviewの要素数変更時にsetLayoutParamsをすること
        View listItem = incomeListView.getAdapter().getView(0, null, incomeListView);
        listItem.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        // listitemは使いまわしなのでincomeのものをどちらのlistviewでも使用
        listItemHeight = listItem.getMeasuredHeight();
        incomeListWrapper = (LinearLayout) v.findViewById(R.id.income_list_wrapper);
        expensesListWrapper = (LinearLayout) v.findViewById(R.id.expenses_list_wrapper);
        changeIncomeListSize();
        changeExpensesListSize();
        updateIncomeTotal();
        updateExpensesTotal();

        //int wrapperHeight = listItemHeight * incomeListView.getCount();
        //incomeListWrapper.setLayoutParams(new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, wrapperHeight));
        // TODO 共通化が可能
        incomeAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 一番最後のカテゴリをlistに追加
                if(incomeList.size() != 0) {
                    // 最後の項目が未入力だった場合追加はせずreturn
                    if(incomeList.get(incomeList.size() - 1).isEmpty()) {
                        Toast.makeText(context, "未入力の項目があります", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        incomeCatList = updateCategory(incomeList, incomeCatList);
                    }
                }
                DataDetail detail = new DataDetail();
                detail.setDay(day);
                detail.setMonth(month);
                detail.setYear(year);
                incomeList.add(detail);
                incomeListAdapter.notifyDataSetChanged();
                changeIncomeListSize();
            }
        });

        expensesAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(expensesList.size() != 0) {
                    // 最後の項目が未入力だった場合追加はせずreturn
                    if(expensesList.get(expensesList.size() - 1).isEmpty()) {
                        Toast.makeText(context, "未入力の項目があります", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        expensesCatList = updateCategory(expensesList, expensesCatList);
                    }
                }
                DataDetail detail = new DataDetail();
                detail.setDay(day);
                detail.setMonth(month);
                detail.setYear(year);
                expensesList.add(detail);
                expensesListAdapter.notifyDataSetChanged();
                changeExpensesListSize();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO 連打対策
                if (incomeList.size() > 0 || expensesList.size() > 0) {
                    String table = ConstDef.TABLE_NAME;
                    DBOpenHelper helper = new DBOpenHelper(context, table);
                    SQLiteDatabase db = helper.getWritableDatabase();
                    DBManager manager = new DBManager(db, table);
                    if (incomeList.size() > 0) {
                        manager.insertData(incomeList, ConstDef.DB_TYPE_INCOME);

                        incomeCatList = updateCategory(incomeList, incomeCatList);
                        SharedPreferencesUtil.saveCategory(context, incomeCatList, ConstDef.PREF_IN_CATEGORY);
                    }
                    if (expensesList.size() > 0) {
                        manager.insertData(expensesList, ConstDef.DB_TYPE_EXPENSES);

                        expensesCatList = updateCategory(expensesList, expensesCatList);
                        SharedPreferencesUtil.saveCategory(context, expensesCatList, ConstDef.PREF_EX_CATEGORY);
                    }
                    Toast.makeText(context, "保存完了しました", Toast.LENGTH_SHORT).show();
                    db.close();
                } else {
                    Toast.makeText(context, "＋ボタンから新規項目を追加して下さい", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    /**
     * incomeListサイズ変更処理
     */
    private void changeIncomeListSize() {
        int wrapperHeight = listItemHeight * incomeListView.getCount();
        incomeListWrapper.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, wrapperHeight));
    }

    /**
     * incomeListサイズ変更処理
     */
    private void changeExpensesListSize() {
        int wrapperHeight = listItemHeight * expensesListView.getCount();
        expensesListWrapper.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, wrapperHeight));
    }

    /**
     * カテゴリーリストの更新 もう少しスマートにやれる気がする
     * @param dataList データのlist
     * @param catList カテゴリーのリスト
     * @return 更新したカテゴリーリスト
     */
    private ArrayList<String> updateCategory(ArrayList<DataDetail> dataList, ArrayList<String> catList) {
        catList.add(dataList.get(dataList.size() - 1).getCategory());
        Set<String> set = new HashSet<>(catList);
        return catList = new ArrayList<>(set);
    }

    /**
     * list末尾の要素の未入力チェック
     * @return true:未入力有り false:なし
     */
    private boolean isLastEmpty(ArrayList<DataDetail> array) {
        if (array.size() != 0) {
            if (array.get(array.size() - 1).isEmpty()) {
                //Toast.makeText(context, "未入力の項目があります", Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return false;
    }

    private void updateIncomeTotal() {
        sumIncome = 0;
        for (DataDetail d : incomeList) {
            sumIncome += Integer.parseInt(d.getValue());
        }
        incomeTotal.setText(getString(R.string.money, sumIncome));
    }

    private void updateExpensesTotal() {
        sumExpenses = 0;
        for (DataDetail d : expensesList) {
            sumExpenses += Integer.parseInt(d.getValue());
        }
        expensesTotal.setText(getString(R.string.money, sumExpenses));
    }

    // 収入リスト
    private class IncomeListAdapter extends BaseAdapter {
        private LayoutInflater inflater;

        public IncomeListAdapter(Context c) {
            inflater = LayoutInflater.from(c);
        }

        @Override
        public int getCount() {
            synchronized (incomeList) {
                if (incomeList.size() > 0) {
                    return incomeList.size();
                } else {
                    return 0;
                }
            }
        }

        @Override
        public Object getItem(int position) {
            return incomeList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ItemHolder holder;
            // パーティーメンバーリスト用アイテムと接続
            if(convertView == null) {
                convertView = inflater.inflate(R.layout.input_list_item, null);
            }
            holder = new ItemHolder();
            holder.categoryInput = (EditText) convertView.findViewById(R.id.category_input);
            holder.moneyInput = (EditText) convertView.findViewById(R.id.money_input);
            holder.categoryListButton = (ImageView) convertView.findViewById(R.id.category_list_button);
            holder.deleteButton = (ImageView) convertView.findViewById(R.id.delete_button);

            if (incomeList.size() > 0) {
                holder.categoryInput.setText(incomeList.get(position).getCategory());
                holder.moneyInput.setText(incomeList.get(position).getValue());
            }

            holder.categoryInput.setTag(position);
            holder.categoryInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        /*
                        clickEditFocusDisable(v);
                        v.setFocusable(false);
                        v.setFocusableInTouchMode(false);
                        */
                        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        int position = (int) v.getTag();
                        if (!TextUtils.isEmpty(v.getText().toString())) {
                            incomeList.get(position).setCategory(v.getText().toString());
                        }
                        handled = true;
                    }
                    return handled;
                }
            });

            holder.moneyInput.setTag(position);
            holder.moneyInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        /*
                        clickEditFocusDisable(v);
                        v.setFocusable(false);
                        v.setFocusableInTouchMode(false);
                        */
                        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        int position = (int) v.getTag();
                        if (!TextUtils.isEmpty(v.getText().toString())) {
                            incomeList.get(position).setValue(v.getText().toString());
                            updateIncomeTotal();
                        }
                        handled = true;
                    }
                    return handled;
                }
            });

            holder.categoryListButton.setTag(holder);
            holder.categoryListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final ItemHolder selectHolder = (ItemHolder) v.getTag();
                    String title = "カテゴリー選択";
                    String[] array = incomeCatList.toArray(new String[incomeCatList.size()]);
                    AlertDlg dlg = new AlertDlg(context, title, array,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO 決定キー押下が必要になってしまう
                                    selectHolder.categoryInput.setText(incomeCatList.get(which));
                                }
                            });
                    dlg.showSelectDialog();
                }
            });

            holder.deleteButton.setTag(position);
            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // リストから削除
                    int index = (int) v.getTag();
                    String deleteValue = incomeList.get(index).getValue();
                    if (!TextUtils.isEmpty(deleteValue)) {
                        sumIncome -= Integer.parseInt(deleteValue);
                        incomeTotal.setText(getString(R.string.money, sumIncome));
                    }
                    incomeList.remove(index);
                    incomeListAdapter.notifyDataSetChanged();
                    changeIncomeListSize();
                }
            });

            return convertView;
        }
    }

    // 支出リスト
    private class ExpensesListAdapter extends BaseAdapter {
        private LayoutInflater inflater;

        public ExpensesListAdapter(Context c) {
            inflater = LayoutInflater.from(c);
        }

        @Override
        public int getCount() {
            synchronized (expensesList) {
                if (expensesList.size() > 0) {
                    return expensesList.size();
                } else {
                    return 0;
                }
            }
        }

        @Override
        public Object getItem(int position) {
            return expensesList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ItemHolder holder;
            // パーティーメンバーリスト用アイテムと接続
            if(convertView == null) {
                convertView = inflater.inflate(R.layout.input_list_item, null);
            }
            holder = new ItemHolder();
            holder.categoryInput = (EditText) convertView.findViewById(R.id.category_input);
            holder.moneyInput = (EditText) convertView.findViewById(R.id.money_input);
            holder.categoryListButton = (ImageView) convertView.findViewById(R.id.category_list_button);
            holder.deleteButton = (ImageView) convertView.findViewById(R.id.delete_button);
            if (expensesList.size() > 0) {
                holder.categoryInput.setText(expensesList.get(position).getCategory());
                holder.moneyInput.setText(expensesList.get(position).getValue());
            }

            holder.categoryInput.setTag(position);
            holder.categoryInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {

                        //clickEditFocusDisable(v);
                        // v.setFocusable(false);
                        //v.setFocusableInTouchMode(false);
                        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        // TODO キャンセルボタン等でキーボードを閉じた場合がアレ フォーカス外されたことをハンドリング？
                        int position = (int) v.getTag();
                        if (!TextUtils.isEmpty(v.getText().toString())) {
                            expensesList.get(position).setCategory(v.getText().toString());
                        }
                        handled = true;
                    }
                    return handled;
                }
            });

            holder.moneyInput.setTag(position);
            holder.moneyInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        /*
                        clickEditFocusDisable(v);
                        v.setFocusable(false);
                        v.setFocusableInTouchMode(false);
                        */
                        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        int position = (int) v.getTag();
                        if (!TextUtils.isEmpty(v.getText().toString())) {
                            expensesList.get(position).setValue(v.getText().toString());
                            updateExpensesTotal();
                        }
                        handled = true;
                    }
                    return handled;
                }
            });

            holder.categoryListButton.setTag(holder);
            holder.categoryListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final ItemHolder selectHolder = (ItemHolder) v.getTag();
                    String title = "カテゴリー選択";
                    String[] array = expensesCatList.toArray(new String[expensesCatList.size()]);
                    AlertDlg dlg = new AlertDlg(context, title, array,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO 決定キー押下が必要になってしまう
                                    selectHolder.categoryInput.setText(expensesCatList.get(which));
                                }
                            });
                    dlg.showSelectDialog();
                }
            });

            holder.deleteButton.setTag(position);
            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // リストから削除
                    int index = (int) v.getTag();
                    String deleteValue = expensesList.get(index).getValue();
                    if (!TextUtils.isEmpty(deleteValue)) {
                        sumExpenses -= Integer.parseInt(deleteValue);
                        expensesTotal.setText(getString(R.string.money, sumExpenses));
                    }
                    expensesList.remove(index);
                    expensesListAdapter.notifyDataSetChanged();
                    changeExpensesListSize();
                }
            });

            return convertView;
        }
    }

    private class ItemHolder {
        EditText categoryInput;
        EditText moneyInput;
        ImageView categoryListButton;
        ImageView deleteButton;
    }

}
