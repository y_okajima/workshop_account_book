package com.key.pleinair.accountbook.utils;

/**
 * Created by kara-ag on 2017/03/12.
 */

public interface OnListChangeListener {
    void onListSelectedChanged(String s);
}