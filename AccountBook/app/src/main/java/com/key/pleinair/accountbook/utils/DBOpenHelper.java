package com.key.pleinair.accountbook.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * DBオープンクラス
 */
public class DBOpenHelper extends SQLiteOpenHelper {
    private Context context;
    private String create;
    private static final String TAG = DBOpenHelper.class.getSimpleName();
    private static final String DB_NAME = "AccountBook.db";
    private static final int DB_VERSION = 1;
    // テーブル名はBOP_userID
    public DBOpenHelper(final Context context, String tableName) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        // date型がないため数字で分割して保存 TODO まとめた方がいいがどうするか
        this.create = "CREATE TABLE " + tableName + " ( " +
                "_id integer primary key autoincrement, " +
                "category text, " +
                "value integer, " +
                "year integer," +
                "month integer, " +
                "day integer, " +
                "type integer);";

    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        Log.d(TAG, "onCreate version : " + db.getVersion());
        Log.d(TAG, create);
        db.execSQL(create);
        //this.execFileSQL(db, "create_table.sql");
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        Log.d(TAG, "onUpgrade version : " + db.getVersion());
        Log.d(TAG, "onUpgrade oldVersion : " + oldVersion);
        Log.d(TAG, "onUpgrade newVersion : " + newVersion);
        //if(oldVersion < newVersion){
        //    db.execSQL(create);
        //}
    }

    /**
     * assetsフォルダのSQLファイルを実行する
     * @param db
     * @param fileName
     */
    private void execFileSQL(SQLiteDatabase db, String fileName){
        InputStream in = null;
        InputStreamReader inReader = null;
        BufferedReader reader = null;
        try {
            // 文字コード(UTF-8)を指定して、ファイルを読み込み
            in = context.getAssets().open(fileName);
            inReader = new InputStreamReader(in, "UTF-8");
            reader = new BufferedReader(inReader);

            // ファイル内の全ての行を処理
            String s;
            while((s = reader.readLine()) != null){
                // 先頭と末尾の空白除去
                s = s.trim();

                // 文字が存在する場合（空白行は処理しない）
                if (0 < s.length()){
                    // SQL実行
                    db.execSQL(s);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (inReader != null) {
                try {
                    inReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
