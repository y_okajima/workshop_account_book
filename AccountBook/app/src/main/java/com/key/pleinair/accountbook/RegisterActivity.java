package com.key.pleinair.accountbook;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.key.pleinair.accountbook.utils.ConstDef;
import com.key.pleinair.accountbook.utils.SharedPreferencesUtil;

import java.util.Map;

/**
 * Created by okajima on 2017/03/06.
 */
public class RegisterActivity extends Activity {
    private static final int REQUEST_READ_CONTACTS = 0;
    // TODO 戻るボタン
    private Map<String, String> userMap;
    private EditText idView;
    private EditText passwordView;
    private EditText reInputPasswordView;
    private View progressView;
    private View loginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_account_activity);
        userMap = SharedPreferencesUtil.getUser(getApplicationContext());
        // Set up the login form.
        idView = (EditText) findViewById(R.id.id);
        passwordView = (EditText) findViewById(R.id.password);
        reInputPasswordView = (EditText) findViewById(R.id.re_password);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button signInButton = (Button) findViewById(R.id.register_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        loginFormView = findViewById(R.id.login_form);
        progressView = findViewById(R.id.login_progress);
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */ // TODO エラーのセット
    private void attemptLogin() {
        // TODO 空白の除去は必要？
        // Reset errors.
        idView.setError(null);
        passwordView.setError(null);

        // Store values at the time of the login attempt.
        String id = idView.getText().toString();
        String password = passwordView.getText().toString();
        String rePassword = reInputPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            focusView = passwordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(id)) {
            idView.setError(getString(R.string.error_field_required));
            focusView = idView;
            cancel = true;
        }

        // Check for a password re-input
        if (!password.equals(rePassword)) {
            reInputPasswordView.setError(getString(R.string.error_incorrect_re_password));
            focusView = reInputPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // TODO そのままログインした方がいいのでは？ あと既に存在してる場合pass上書きになるけど本当にいいのか？
            SharedPreferencesUtil.saveUser(getApplicationContext(), id, password);
            Intent intent = new Intent(this, LoginActivity.class);
            //intent.putExtra(ConstDef.INTENT_USER, id);
            finish();
            startActivity(intent);
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
}
