package com.key.pleinair.accountbook;


import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.TextViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.key.pleinair.accountbook.utils.ConstDef;
import com.key.pleinair.accountbook.utils.DBManager;
import com.key.pleinair.accountbook.utils.DBOpenHelper;

import org.w3c.dom.Text;

import java.util.Calendar;


/**
 *
 */
public class CalendarFragment extends Fragment {
    private String userId = "";
    private String selectDate;
    private int selectYear = 0;
    private int selectMonth = 0;
    private int selectDay = 0;
    private Button detailButton;
    private TextView dayIncomeTotal;
    private TextView dayExpensesTotal;
    private TextView monthIncomeTotal;
    private TextView monthExpensesTotal;
    private TextView bopTotalTitle;
    private TextView bopTotal;
    public CalendarFragment() {
    }

    public static CalendarFragment newInstance(Bundle bundle) {
        CalendarFragment fragment = new CalendarFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.calendar_fragment, container, false);
        // 現在の日付をを初期値にする monthは1月が0からなので+1
        Calendar now = Calendar.getInstance();
        selectYear = now.get(now.YEAR);
        selectMonth = now.get(now.MONTH) + 1;
        selectDay = now.get(now.DAY_OF_MONTH);
        selectDate = selectYear+"年"+selectMonth+"月"+selectDay+"日";

        //userId = ConstDef.USER_ID;

        CalendarView cal = (CalendarView) v.findViewById(R.id.calendarView);
        detailButton = (Button) v.findViewById(R.id.detail_button);
        dayIncomeTotal = (TextView) v.findViewById(R.id.day_income_total);
        dayExpensesTotal = (TextView) v.findViewById(R.id.day_expenses_total);
        //TextView userNameText = (TextView) v.findViewById(R.id.user_name);
        //userNameText.setText(userId);
        monthIncomeTotal = (TextView) v.findViewById(R.id.month_income_total);
        monthExpensesTotal = (TextView) v.findViewById(R.id.month_expenses_total);
        bopTotalTitle = (TextView) v.findViewById(R.id.bop_total_text);
        bopTotal = (TextView) v.findViewById(R.id.bop_total);

        getSelectDayBOP();
        getSelectMonthBOP();

        cal.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                selectYear = year;
                selectMonth = month + 1;
                selectDay = dayOfMonth;
                selectDate = selectYear+"年"+selectMonth+"月"+selectDay+"日";
                getSelectDayBOP();
                getSelectMonthBOP();
                // TODO ロングタップで遷移できるといいかも
            }
        });

        detailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 選択中の日付の詳細入力へ遷移
                Bundle bundle = new Bundle();
                bundle.putString(ConstDef.INTENT_DATE, selectDate);
                bundle.putInt(ConstDef.INTENT_YEAR, selectYear);
                bundle.putInt(ConstDef.INTENT_MONTH, selectMonth);
                bundle.putInt(ConstDef.INTENT_DAY, selectDay);
                InputDetailFragment fragment = new InputDetailFragment().newInstance(bundle);
                FragmentManager manager = getActivity().getSupportFragmentManager();
                manager.beginTransaction()
                        .replace(R.id.fragment_frame, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return v;
    }

    /**
     * 選択した日付の収支を取得し表示
     */
    private void getSelectDayBOP() {
        String table = ConstDef.TABLE_NAME;
        DBOpenHelper helper = new DBOpenHelper(getActivity(), table);
        SQLiteDatabase db = helper.getReadableDatabase();
        DBManager manager = new DBManager(db, table);
        String income = manager.sumDay(selectYear, selectMonth, selectDay, ConstDef.DB_TYPE_INCOME);
        String expenses = manager.sumDay(selectYear, selectMonth, selectDay, ConstDef.DB_TYPE_EXPENSES);
        dayIncomeTotal.setText(getString(R.string.money, Integer.parseInt(income)));
        dayExpensesTotal.setText(getString(R.string.money, Integer.parseInt(expenses)));
        db.close();
    }

    /**
     * 選択している月の収支を取得し表示
     */
    private void getSelectMonthBOP() {
        String table = ConstDef.TABLE_NAME;
        DBOpenHelper helper = new DBOpenHelper(getActivity(), table);
        SQLiteDatabase db = helper.getReadableDatabase();
        DBManager manager = new DBManager(db, table);
        String income = manager.sumMonth(selectYear, selectMonth, ConstDef.DB_TYPE_INCOME);
        String expenses = manager.sumMonth(selectYear, selectMonth, ConstDef.DB_TYPE_EXPENSES);

        monthIncomeTotal.setText(getString(R.string.money, Integer.parseInt(income)));
        monthExpensesTotal.setText(getString(R.string.money, Integer.parseInt(expenses)));
        bopTotal.setText(getString(R.string.money, Integer.parseInt(income) - Integer.parseInt(expenses)));
        bopTotalTitle.setText(getString(R.string.bop_total_title, selectMonth));

        db.close();

    }

}
