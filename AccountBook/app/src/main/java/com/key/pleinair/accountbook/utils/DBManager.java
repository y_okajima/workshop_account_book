package com.key.pleinair.accountbook.utils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * DB操作クラス
 */
public class DBManager {
    private SQLiteDatabase db;
    private String table;

    private static final int CURSOR_ID = 0;
    private static final int CURSOR_CATEGORY = 1;
    private static final int CURSOR_VALUE = 2;
    private static final int CURSOR_MONTH = 3;
    private static final int CURSOR_DAY = 4;
    private static final int CURSOR_TYPE = 5;
    private static final String TAG = DBManager.class.getSimpleName();
    public DBManager(SQLiteDatabase db, String table) {
        this.db = db;
        this.table = table;
    }
    // とりあえず必要そうなメソッドを列挙 使うかどうかは置いといて処理は書こう

    /**
     * データのinsert
     * @param data
     * @param type 1:収入 2:支出
     * @return true:成功 false:失敗
     */
    public void insertData(ArrayList<DataDetail> data, int type) {
        for(DataDetail d : data) {
            if (!d.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put(ConstDef.COLUMN_CATEGORY, d.getCategory());
                values.put(ConstDef.COLUMN_VALUE, Integer.parseInt(d.getValue()));
                //values.put(ConstDef.COLUMN_DATE, d.getDate());
                values.put(ConstDef.COLUMN_YEAR, d.getYear());
                values.put(ConstDef.COLUMN_MONTH, d.getMonth());
                values.put(ConstDef.COLUMN_DAY, d.getDay());
                values.put(ConstDef.COLUMN_TYPE, type);

                long ret;
                ret = db.insert(table, null, values);
                if (ret != -1) {
                    Log.d(TAG, "insert success");
                } else {
                    Log.d(TAG, "insert failure");
                }
            } else {
                Log.d(TAG, "has empty data");
            }
        }
        //クローズは呼び出し元にさせる
        //db.close();
    }

    // TODO 難点 更新日付のデータを全部消すか？とりあえず保留で
    public void updateData() {

    }

    /**
     * 指定した日付とタイプのデータを取得する
     * @param year
     * @param month
     * @param day
     * @param type
     * @return
     */
    public ArrayList<DataDetail> selectDay(int year, int month, int day, int type) {
        String query = String.format("SELECT * FROM %s " +
                "WHERE year = %s AND month = %s AND day = %s AND type = %s;", table, year, month, day, type);
        Log.d(TAG, query);
        Cursor cursor = db.rawQuery(query, null);

        ArrayList<DataDetail> dataArray = new ArrayList<>();
        while(cursor.moveToNext()) {
            //必要な値を取り出す
            DataDetail data = new DataDetail();
            data.setCategory(cursor.getString(CURSOR_CATEGORY));
            data.setValue(String.valueOf(cursor.getInt(CURSOR_VALUE)));
            data.setMonth(cursor.getInt(CURSOR_MONTH));
            data.setDay(cursor.getInt(CURSOR_DAY));
            dataArray.add(data);
        }
        cursor.close();

        return dataArray;
    }

    /**
     * 指定した日付とタイプのデータを取得する
     * @param year
     * @param month
     * @param type
     * @return
     */
    public ArrayList<DataDetail> selectMonth(int year, int month, int type) {
        String query = String.format("SELECT * FROM %s " +
                "WHERE year = %s AND month = %s AND type = %s;", table, year, month, type);
        Log.d(TAG, query);
        Cursor cursor = db.rawQuery(query, null);

        ArrayList<DataDetail> dataArray = new ArrayList<>();
        while(cursor.moveToNext()) {
            //必要な値を取り出す
            DataDetail data = new DataDetail();
            data.setId(cursor.getInt(CURSOR_ID));
            data.setCategory(cursor.getString(CURSOR_CATEGORY));
            data.setValue(String.valueOf(cursor.getInt(CURSOR_VALUE)));
            data.setMonth(cursor.getInt(CURSOR_MONTH));
            data.setDay(cursor.getInt(CURSOR_DAY));
            dataArray.add(data);
        }
        cursor.close();

        return dataArray;
    }


    /*
    public String sumCategory(String category) {
    }

    public String sumAll() {
        //いらなそう
    }
    */

    /**
     * 指定した日付とタイプのvalueの合計値を取得する
     * @param year
     * @param month
     * @param day
     * @param type
     * @return
     */
    public String sumDay(int year, int month, int day, int type) {
        String query = String.format("SELECT SUM(value) FROM %s " +
                "WHERE year = %s AND month = %s AND day = %s AND type = %s;", table, year, month, day, type);
        Log.d(TAG, query);
        Cursor cursor = db.rawQuery(query, null);
        int point = 0;
        if(cursor.moveToNext()){
            point = cursor.getInt(0);
            Log.d(TAG, String.valueOf(point));
        }
        cursor.close();

        return String.valueOf(point);
    }

    /**
     * 指定した月とタイプのvalueの合計値を取得する
     * @param year
     * @param month
     * @param type
     * @return
     */
    public String sumMonth(int year, int month, int type) {
        String query = String.format("SELECT SUM(value) FROM %s " +
                "WHERE year = %s AND month = %s AND type = %s ;", table, year, month, type);
        Log.d(TAG, query);
        Cursor cursor = db.rawQuery(query, null);
        int point = 0;
        if(cursor.moveToNext()){
            point = cursor.getInt(0);
            Log.d(TAG, String.valueOf(point));
        }
        cursor.close();

        return String.valueOf(point);
    }

    public void deleteData(int id) {
        Log.d(TAG, "delete id = " + id);//new String[]{String.valueOf(id)}
        db.delete(table, "_id = ?", new String[]{String.valueOf(id)});
    }

    /*
    public void deleteTable() {

    }

    public void deleteDB() {

    }
    */

}
