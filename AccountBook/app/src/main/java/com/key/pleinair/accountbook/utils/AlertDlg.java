package com.key.pleinair.accountbook.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 *
 */

public class AlertDlg {
    private static final String BUTTON_OK = "OK";
    private static final String BUTTON_CANCEL = "キャンセル";
    private AlertDialog dialog;

    Context context;
    String title;
    String message;
    String[] items;
    DialogInterface.OnClickListener okListener;
    DialogInterface.OnClickListener cancelListener;

    /**
     * 通常ダイアログコンストラクタ
     * @param context コンテキスト
     * @param title タイトル
     * @param message メッセージ
     * @param ok OKボタンクリック時の処理
     * @param cancel キャンセルボタンクリック時の処理
     */
    public AlertDlg(Context context, String title, String message, DialogInterface.OnClickListener ok, DialogInterface.OnClickListener cancel){
        this.context = context;
        this.title = title;
        this.message = message;
        okListener = ok;
        cancelListener = cancel;
    }

    /**
     * 選択ダイアログコンストラクタ
     * @param context コンテキスト
     * @param title タイトル
     * @param items 表示アイテム
     * @param ok 選択決定時のリスナー
     */
    public AlertDlg(Context context, String title, String[] items, DialogInterface.OnClickListener ok){
        this.context = context;
        this.title = title;
        this.items = items;
        okListener = ok;
    }

    /**
     * ダイアログ表示
     */
    public void showDialog(){
        // ダイアログの生成
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(context);
        alertDlg.setTitle(title);
        alertDlg.setMessage(message);

        alertDlg.setPositiveButton(
                BUTTON_OK,
                okListener);
        if(cancelListener != null) {
            alertDlg.setNegativeButton(
                    BUTTON_CANCEL,
                    cancelListener);
        }

        // 表示
        dialog = alertDlg.create();
        dialog.show();
    }

    /**
     * ボタンを押さないと消せないダイアログを表示
     */
    public void showIndelibleDialog() {
        // ダイアログの生成
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(context);
        alertDlg.setTitle(title);
        alertDlg.setMessage(message);
        alertDlg.setCancelable(false);

        alertDlg.setPositiveButton(
                BUTTON_OK,
                okListener);
        if(cancelListener != null) {
            alertDlg.setNegativeButton(
                    BUTTON_CANCEL,
                    cancelListener);
        }

        // 表示
        dialog = alertDlg.create();
        dialog.show();
    }

    /**
     * 選択ダイアログを表示
     */
    public void showSelectDialog() {
        // ダイアログの生成
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(context);
        alertDlg.setTitle(title);
        alertDlg.setItems(items, okListener);

        dialog = alertDlg.create();
        dialog.show();
    }

    /**
     * ダイアログ表示状況取得
     * @return ダイアログ表示中/非表示
     */
    public boolean isShowing() {
        if(dialog != null && dialog.isShowing()) {
            return true;
        }
        return false;
    }
}
