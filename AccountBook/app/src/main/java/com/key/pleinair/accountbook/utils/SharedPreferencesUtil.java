package com.key.pleinair.accountbook.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * SharedPreferences操作クラス
 */
public class SharedPreferencesUtil {
    // キー
    private static final String
            PREF = "account_book";

    private static final String TAG = SharedPreferencesUtil.class.getSimpleName();

    /**
     * カテゴリ保存
     *
     * @param context
     * @param array
     * @param PrefKey
     */
    public static void saveCategory(Context context, ArrayList<String> array, String PrefKey) {
        Gson gson = new Gson();
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF, Context.MODE_PRIVATE).edit();
        editor.putString(PrefKey, gson.toJson(array));
        editor.commit();
    }

    /**
     * カテゴリよみ出し
     * @param context
     * @param PrefKey
     * @return
     */
     public static ArrayList<String> getCategory(Context context, String PrefKey){
        Gson gson = new Gson();
        SharedPreferences prefs = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        String stringItem = prefs.getString(PrefKey, "");
        ArrayList<String> catArray = gson.fromJson(stringItem, new TypeToken<ArrayList<String>>(){}.getType());
        if (catArray == null) {
            catArray = new ArrayList<>();
        }
        return catArray;
    }

    /**
     * ユーザー保存
     * @param context
     * @param id
     * @param password
     */
    public static void saveUser(Context context, String id, String password) {
        Gson gson = new Gson();
        Map<String, String> newMap = getUser(context);
        newMap.put(id, password);
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF, Context.MODE_PRIVATE).edit();
        editor.putString(ConstDef.PREF_USER, gson.toJson(newMap));
        editor.commit();
    }

    /**
     * ユーザーの読み込み
     * @param context
     * @return
     */
    public static Map<String, String> getUser(Context context){
        Gson gson = new Gson();
        SharedPreferences prefs = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        String stringItem = prefs.getString(ConstDef.PREF_USER, "");
        Map<String, String> map = gson.fromJson(stringItem, new TypeToken<Map<String ,String>>(){}.getType());
        if (map == null) {
            map = new HashMap<String, String>();
        }
        return map;
    }
}
