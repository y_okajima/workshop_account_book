package com.key.pleinair.accountbook;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.key.pleinair.accountbook.utils.ConstDef;

import org.w3c.dom.Text;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String userId = "";
    private int year = 0;
    private int month = 0;
    private int day = 0;

    private static final String TAG = MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        userId = intent.getStringExtra(ConstDef.INTENT_USER);
        ConstDef.setUserId(userId);

        // カレンダーフラグメントの起動
        Bundle bundle = new Bundle();
        bundle.putString(ConstDef.INTENT_USER, userId);
        CalendarFragment fragment = new CalendarFragment().newInstance(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        TextView headerName = (TextView) headerView.findViewById(R.id.header_name);
        TextView headerDate = (TextView) headerView.findViewById(R.id.header_date);
        headerName.setText(userId + "さん");
        Calendar now = Calendar.getInstance();
        year = now.get(now.YEAR);
        month = now.get(now.MONTH) + 1;
        day = now.get(now.DAY_OF_MONTH);
        headerDate.setText("今日は " + month  + "月" + day + "日");

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_calendar) {
            // カレンダーフラグメントの起動
            CalendarFragment fragment = new CalendarFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment).commit();
        } else if (id == R.id.nav_month_bop) {
            Bundle bundle = new Bundle();
            bundle.putInt(ConstDef.INTENT_YEAR, year);
            bundle.putInt(ConstDef.INTENT_MONTH, month);
            MonthBOPListFragment fragment = new MonthBOPListFragment().newInstance(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment).commit();
        } /*else if (id == R.id.nav_cat) {

        } else if (id == R.id.nav_cat_edit) {

        } else if (id == R.id.nav_setting) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
